<?php
// src/AppBundle/Entity/Task.php
namespace Meddo\HealthyBundle\Entity;

class Task
{
    protected $task;
    protected $spec;

    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }

    public function getSpec()
    {
        return $this->spec;
    }

    public function setSpec($spec)
    {
        $this->spec = $spec;
    }
}