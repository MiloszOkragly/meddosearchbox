<?php
// src/Meddo/HealthyBundle/Controller/PageController.php

namespace Meddo\HealthyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Meddo\HealthyBundle\Entity\Doctor;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Meddo\HealthyBundle\Entity\Task;

class PageController extends Controller
{
	

public function newAction(Request $request)
    {
    	$result = 0;
        $formularzyk = new Task();


        $form = $this->get('form.factory')->createNamedBuilder('', 'form', $formularzyk, array('csrf_protection' => false,) )
        	//->setAction($this->generateUrl('MeddoHealthyBundle_display'))
			->setMethod('GET')
            ->add('task', 'choice', array('choices' => array(
            'gdansk' => 'Gdańsk', 
            'gdynia' => 'Gdynia', 
            'sopot' => 'Sopot'),
			'required' => true, 
			'placeholder' => 'Wybierz miasto', 
			'label' => false))
            ->add('spec', 'choice', array('choices' => array(
            'ginekolog' => 'Ginekolog', 
            'alergolog' => 'Alergolog', 
            'chirurg' => 'Chirurg',
			'kardiolog' => 'Kardiolog',
			'okulista' => 'Okulista',
			'ortopeda' => 'Ortopeda'),
			'required' => true, 
			'placeholder' => 'Wybierz specjalizację', 
			'label' => false))
            ->add('save', 'submit', array('label' => 'Szukaj'))
            ->getForm();
			
 $form->handleRequest($request);			
 
if ($request->getMethod() == 'GET') {
    if ($form->isValid()) {
    	
$city = $_GET['task'];
$job = ucfirst($_GET['spec']);	
	

if ($city=='gdansk'){
	
	$city ='Gdańsk';

}

if ($city=='sopot'){

	$city = 'Sopot';
}

if ($city=='gdynia'){
	
	$city = 'Gdynia';
}

$doctor = $this->getDoctrine()
       ->getRepository('MeddoHealthyBundle:Doctor')
       ->findBy(array('specialization' => $job,
	   		   		  'town' => $city)
			   );
$result = count($doctor);

  if (!$doctor) {
        //throw $this->createNotFoundException(
          //  'Nie znaleziono żadnych wyników');
          return $this->render('MeddoHealthyBundle:Page:display.html.twig', array('doctor'=>$doctor, 'form' => $form->createView(),
	'result' => $result));
		  }
    return $this->render('MeddoHealthyBundle:Page:display.html.twig', array('doctor'=>$doctor, 'form' => $form->createView(),
	'result' => $result));

	}
		
       
    }
	

        return $this->render('MeddoHealthyBundle:Page:index.html.twig', array('form' => $form->createView(),
	'result' => $result));

				
    }
 
 public function displayAction(Request $request){


    return $this->render('MeddoHealthyBundle:Page:display.html.twig');

 }
 
 
 public function uploadAction(Request $request)
{
    $doctor = new Doctor();
    $form = $this->createFormBuilder($doctor)
        ->add('name')
		->add('surname')
		->add('specialization')
		->add('description')
		->add('town')
		->add('street')
		->add('rating')
		->add('tel')
        ->add('file')
		->add('agency')
		->add('save', 'submit', array('label' => 'Stwórz rekord'))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
		
		 $doctor->upload();

        $em->persist($doctor);
        $em->flush();
   

        return $this->render('MeddoHealthyBundle:Page:dodawanko.html.twig', array('form' => $form->createView()));
    }

    return $this->render('MeddoHealthyBundle:Page:dodawanko.html.twig', array('form' => $form->createView()));
}
 

}